def source_paths
  [File.expand_path(File.dirname(__FILE__))]
end

def add_gems
  gem_group :development do
    gem "guard-livereload", "~> 2.5", require: false
  end
  # gem "devise", "~> 4.7", ">= 4.7.1"
end

def add_users
  # Install Devise
  generate "devise:install"

  # Configure Devise
  environment "config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }",
    env: "development"

  #  route "root to: 'home#index'"

  # Create Devise User
  generate :devise, "User", "username", "name", "admin:boolean"

  # set admin boolean to false by default
  in_root do
    migration = Dir.glob("db/migrate/*").max_by { |f| File.mtime(f) }
    gsub_file migration, /:admin/, ":admin, default: false"
  end
end

source_paths
add_gems

after_bundle do
  run "spring stop" # Fix hang when generating controller
  run "guard init livereload"

  run "rm -rf app/assets/stylesheets"

  directory "app", force: true

  inject_into_file "app/javascript/packs/application.js" do
    <<~EOF
      import "../stylesheets/application.css"
      import "../font-inter/inter.css"
    EOF
  end

  gsub_file "app/views/layouts/application.html.erb", "stylesheet_link_tag", "stylesheet_pack_tag"

  run "yarn add tailwindcss"
  run "yarn add @tailwindcss/custom-forms"
  run "yarn add @fullhuman/postcss-purgecss"
  copy_file "postcss.config.js", force: true
  run "node_modules/.bin/tailwind init app/javascript/stylesheets/tailwind.config.js"
  inject_into_file("app/javascript/stylesheets/tailwind.config.js", %q(require('@tailwindcss/custom-forms'),), after: "plugins: [")

  inject_into_file("app/views/layouts/application.html.erb", %q(<%= render "shared/flash" %>), after: "<body>")
  inject_into_file("app/views/layouts/application.html.erb", %q(<%= render "shared/navbar" %>), after: "<body>")

  git add: "."
  git commit: %Q{ -m 'Initial commit' }

  say
  say "Everything is ready!", :green
  say
  say "Switch to your app by running:"
  say "$ cd #{app_name}", :yellow
  say
  say "Then run:"
  say "$ rails server", :green
end
